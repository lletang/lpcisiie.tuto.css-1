# Tutoriel CSS - Multiple Column Layout

## Auteurs

* Lucas GEHIN
* Gregory BATON
* Guillaume DEGESNE

## Sommaire
* Introduction
* Compatibilité
* Liste des propriétés
* Exemples
* Sources

## Introduction
Le "multiple column layout" permet de faire écouter un contenu sur plusieurs colonnes afin de réaliser un affichage du même type qu'un journal papier. Ainsi, les personnes habituées à lire des informations sur un journal se retrouvent dans le même environnement, ce qui peut être judicieux sur un site d'actualité par exemple.

Le format final est donc plus facile à lire, notamment sur les petits terminaux comme les smartphones.

Cette propriété est considérée comme stable par le W3C depuis avril 2011.

## Compatibilité

Le module multiple column layout est assez bien supporté par l'ensemble des navigateurs connus (aussi bien en version mobile ou tablette). 

Le tableau suivant présente la version requise et le préfixe CSS à utiliser pour chaque navigateur.

Navigateur | Version | Préfixe à utiliser 
| :---: | :---: | :---: |
Internet Explorer | 10 ou plus | -
Mozilla Firefox | 2.0 ou plus et mobile | -moz- 
Google Chrome | 4.0 ou plus et mobile | -webkit- 
Opera | 11.1 ou plus et mobile | -
Safari | 3.1 ou plus et mobile | -webkit-
Mozilla Firefox | 2.0 ou plus et mobile | -moz- 
Android Browser | 2.1 ou plus | -webkit-  

## Liste des propriétés

### columns
Cette propriété permet de définir la largeur de chaque colonne ainsi que le nombre de colonnes souhaitées.

```
/* Syntaxe */
.texte {
	-prefixe-columns: largeur nombre;}
```

Pour modifier uniquement la largeur ou le nombre de colonnes, on peut utiliser les propriétés ``column-width`` (pour la largeur) et ``column-count`` (pour le nombre/)

### column-gap
Cette propriété permet de définir la taille de l'écart entre les colonnes.

```
/* Syntaxe */
.texte {
	-prefixe-column-gap: largeur;}
```

### column-rule
Cette propriété permet de définir le trait de séparation entre les colonnes. On peut lui définir une largeur, un style ou une couleur.

```
/* Syntaxe */
.texte {
	-prefixe-column-rule: largeur style couleur;}
```

Pour modifier uniquement la largeur, le style ou la couleur, on peut utiliser les propriétés ``column-rule-width`` (pour la largeur), ``column-rule-style`` (pour le style) et ``column-rule-color`` (pour la couleur).

### column-span
Cette propriété permet de définir si un élément s'étale sur une colonne ou l'ensemble des colonnes. Par exemple un titre.

```
/* Syntaxe */
.texte {
	-prefixe-column-span: 1|ALL;}
```

La valeur ``1`` permet à l'élément de s'étaler uniquement sur la première colonne et la valeur ``ALL`` permet à l'élément de s'étaler sur l'ensemble des colonnes.
Cette propriété n'est pas encore disponible sur Mozilla Firefox !

## Exemples

La liste des propriétés listées sont présentées dans le fichier ``multi-column.html`` sous forme d'exemples.

## Souces
Pour nous aider à réaliser ce tutoriel, nous avons utilisé le site du W3C (http://www.w3schools.com/css/css3_multiple_columns.asp) et une présentation du module sur Alsacréations (http://www.alsacreations.com/tuto/lire/1557-les-multicolonnes-en-css3.html).
